#CrossPHP 简洁高效，轻量易扩展的PHP5.3+ 开发框架#

轻量无依赖，HMVC，注释配置，路由别名，PSR标准，Layer布局

骨架下载: [http://git.oschina.net/ideaa/skeleton](http://git.oschina.net/ideaa/skeleton "git.oschina.net/ideaa/skeleton")  
官方网站: [http://www.crossphp.com](http://www.crossphp.com "www.crossphp.com")  
文档地址: [http://document.crossphp.com](http://document.crossphp.com "document.crossphp.com")

框架和骨架默认放在同一目录